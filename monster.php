<?php

/**

AUTHOR: Jonathan TRIEU, EPITECH 2017

**/

	$link = mysqli_connect('localhost', 'root', 'root', '2017_streetrpg') or die("Erreur de connexion au serveur");
	if (!$link)
		echo mysqli_error();


	$id = $_GET['id'];
	echo "ID Monster : " . $id . "<br /><br />";

	// MONSTER TABLE
	$q = mysqli_query($link, "SELECT * FROM monster WHERE id = '" . $id . "'");

	while($e = mysqli_fetch_assoc($q))
		$output = $e;
	$result = json_encode($output);
	$result = str_replace("{", "", $result);
	$result = str_replace("}", "", $result);
	$monster_tabs = explode(",", $result);

	echo "Monster information :<br />";

	foreach ($monster_tabs as $monster_tab) {
		$monster_tab = str_replace('"', "", $monster_tab);
		$split = explode(":", $monster_tab);
    	$monster_array[$split[0]] = $split[1];
    	echo $split[0] . " => " . $split[1] . "<br />";
	}
	echo "<br />";


	// MONSTER SKILL 1

	$q = mysqli_query($link, "SELECT * FROM skill WHERE id = '" . $monster_array["fk_skill_id1"] . "'");
	while($e = mysqli_fetch_assoc($q))
		$output = $e;
	$result = json_encode($output);
	$result = str_replace("{", "", $result);
	$result = str_replace("}", "", $result);
	$monster_skill1_tabs = explode(",", $result);
	array_shift($monster_skill1_tabs);
	foreach ($monster_skill1_tabs as $monster_skill1_tab) {
		$monster_skill1_tab = str_replace('"', "", $monster_skill1_tab);
		$split = explode(":", $monster_skill1_tab);
    	$monster_skill1_array[$split[0]] = $split[1];
		echo $split[0] . " => " . $split[1] . "<br />";
	}
	echo "<br />";

	// MONSTER SKILL 2

	$q = mysqli_query($link, "SELECT * FROM skill WHERE id = '" . $monster_array["fk_skill_id2"] . "'");
	while($e = mysqli_fetch_assoc($q))
		$output = $e;
	$result = json_encode($output);
	$result = str_replace("{", "", $result);
	$result = str_replace("}", "", $result);
	$monster_skill2_tabs = explode(",", $result);
	array_shift($monster_skill2_tabs);
	foreach ($monster_skill2_tabs as $monster_skill2_tab) {
		$monster_skill2_tab = str_replace('"', "", $monster_skill2_tab);
		$split = explode(":", $monster_skill2_tab);
    	$monster_skill2_array[$split[0]] = $split[1];
		echo $split[0] . " => " . $split[1] . "<br />";
	}
	echo "<br />";

	array_pop($monster_array);
	array_pop($monster_array);


	$skills_array[$monster_skill1_array["name"]] = array("damage" => $monster_skill1_array["damage"], "mp_cost" => $monster_skill1_array["mp_cost"], "heal" => $monster_skill1_array["heal"],
												"description" => $monster_skill1_array["description"]);
	$skills_array[$monster_skill2_array["name"]] = array("damage" => $monster_skill2_array["damage"], "mp_cost" => $monster_skill2_array["mp_cost"], "heal" => $monster_skill2_array["heal"],
												"description" => $monster_skill2_array["description"]);
	
	// STATS
	$stats["hp"] = $monster_array['hp'];
	$stats["hp_max"] = $monster_array['hp_max'];
	$stats["attack"] = $monster_array['attack'];
	$stats["exp"] = $monster_array['exp'];
	$stats["gold"] = $monster_array['gold'];

	//PLAYER
	$array = array("name" => $monster_array['name'], "stats" => $stats, "skills" => $skills_array);
	$monster = array("monster" => $array);
	echo "<br />";
	echo json_encode($monster, JSON_FORCE_OBJECT);
	mysqli_close();
?>