<?php

/**

AUTHOR: Jonathan TRIEU, EPITECH 2017

**/

	$link = mysqli_connect('localhost', 'root', 'root', "2017_streetrpg") or die("Erreur de connexion au serveur");
	if (!$link)
		echo mysqli_error();	

	$id = $_GET['id'];
	// echo "ID User : " . $id . "<br /><br />";

	// USER TABLE
	$q = mysqli_query($link, "SELECT * FROM user WHERE id = " . $id . "");
	while($e = mysqli_fetch_assoc($q)) 
		$output = $e;

	$result = json_encode($output);
	$result = str_replace("{", "", $result);
	$result = str_replace("}", "", $result);
	$user_tabs = explode(",", $result);

	// echo "User information :<br />";
	
	foreach ($user_tabs as $user_tab) {
		$user_tab = str_replace('"', "", $user_tab);
		$split = explode(":", $user_tab);
    	$user_array[$split[0]] = $split[1];
    	// echo $split[0] . " => " . $split[1] . "<br />";
	}
	// echo "<br />";

	// USER STATS TABLE

	$q = mysqli_query($link, "SELECT * FROM user_stat WHERE fk_user_id = '" . $user_array['id'] . "'");
	while($e = mysqli_fetch_assoc($q))
		$output = $e;
	$result = json_encode($output);
	$result = str_replace("{", "", $result);
	$result = str_replace("}", "", $result);
	$user_stats_tabs = explode(",", $result);
	array_shift($user_stats_tabs);
	// echo "User stats :<br />";
	
	foreach ($user_stats_tabs as $user_stats_tab) {
		$user_stats_tab = str_replace('"', "", $user_stats_tab);
		$split = explode(":", $user_stats_tab);
    	$user_stats_array[$split[0]] = $split[1];
    	// echo $split[0] . " => " . $split[1] . "<br />";
	}
	// echo "<br />";


	// USER SKILLS TABLE

	$q = mysqli_query($link, "SELECT * FROM user_skill WHERE fk_user_id = '" . $user_array['id'] . "'");
	while($e = mysqli_fetch_assoc($q))
		$output = $e;
	$result = json_encode($output);
	$result = str_replace("{", "", $result);
	$result = str_replace("}", "", $result);
	$user_skills_tabs = explode(",", $result);
	array_shift($user_skills_tabs);
	// echo "User skills :<br />";
	
	foreach ($user_skills_tabs as $user_skills_tab) {
		$user_skills_tab = str_replace('"', "", $user_skills_tab);
		$split = explode(":", $user_skills_tab);
    	$user_skills_array[$split[0]] = $split[1];

    	//EACH SKILL
		$q = mysqli_query($link, "SELECT * FROM skill WHERE id = '" . $split[1] . "'");
		while($e = mysqli_fetch_assoc($q))
			$output = $e;
		$result = json_encode($output);
		$result = str_replace("{", "", $result);
		$result = str_replace("}", "", $result);
		$user_skill_tabs = explode(",", $result);
		array_shift($user_skill_tabs);
		foreach ($user_skill_tabs as $user_skill_tab) {
			$user_skill_tab = str_replace('"', "", $user_skill_tab);
			$split_skill = explode(":", $user_skill_tab);
    		$user_skill_array[$split_skill[0]] = $split_skill[1];
			// echo $split_skill[0] . " => " . $split_skill[1] . "<br />";
		}
		$skills_array[$user_skill_array["name"]] = array("damage" => $user_skill_array["damage"], "mp_cost" => $user_skill_array["mp_cost"], "heal" => $user_skill_array["heal"],
													"description" => $user_skill_array["description"]);
		// echo "<br />";
	}
	// echo "<br />";


	// LOCATION
	$loc["x"] = $user_array['x'];
	$loc["y"] = $user_array['y'];
	
	// STATS
	$stats["hp"] = $user_stats_array['hp'];
	$stats["mp"] = $user_stats_array['mp'];
	$stats["hp_max"] = $user_stats_array['hp_max'];
	$stats["mp_max"] = $user_stats_array['mp_max'];
	$stats["attack"] = $user_stats_array['attack'];
	$stats["def"] = $user_stats_array['def'];
	$stats["lvl"] = $user_stats_array['lvl'];
	$stats["exp"] = $user_stats_array['exp'];
	$stats["gold"] = $user_stats_array['gold'];

	//PLAYER
	$user = array("name" => $user_array['nickname'], "loc" => $loc, "stats" => $stats, "skills" => $skills_array);
	$player = array("player" => $user);
	// echo "<br />";
	echo json_encode($player, JSON_FORCE_OBJECT);
	mysqli_close();
?>