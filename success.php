<?php

/**

AUTHOR: Jonathan TRIEU, EPITECH 2017

**/

	$link = mysqli_connect('localhost', 'root', 'root', '2017_streetrpg') or die("Erreur de connexion au serveur");
	if (!$link)
		echo mysqli_error();


	$id = $_GET['id'];
	echo "ID User : " . $id . "<br /><br />";

	// USER SUCCESS TABLE
	$q = mysqli_query($link, "SELECT * FROM success WHERE fk_user_id = '" . $id . "'");
	while($e = mysqli_fetch_assoc($q))
		$output = $e;
	$result = json_encode($output);
	$result = str_replace("{", "", $result);
	$result = str_replace("}", "", $result);
	$success_tabs = explode(",", $result);

	echo "User Success information :<br />";
	
	foreach ($success_tabs as $success_tab) {
		$success_tab = str_replace('"', "", $success_tab);
		$split = explode(":", $success_tab);
    	$success_array[$split[0]] = $split[1];
    	echo $split[0] . " => " . $split[1] . "<br />";
	}
	echo "<br />";

	//PLAYER
	$array = array("nb_kill" => $success_array['nb_kill'], "quest_done" => $success_array['quest_done']);
	$success = array("player_success" => $array);
	echo "<br />";
	echo json_encode($success, JSON_FORCE_OBJECT);
	mysqli_close();
?>