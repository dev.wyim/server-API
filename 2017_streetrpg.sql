-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u2
-- http://www.phpmyadmin.net
--
-- Host: db-etud.labeip.local:3306
-- Generation Time: Apr 23, 2016 at 11:45 AM
-- Server version: 5.5.47
-- PHP Version: 5.4.45-0+deb7u2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `2017_streetrpg`
--

-- --------------------------------------------------------

--
-- Table structure for table `monster`
--

CREATE TABLE IF NOT EXISTS `monster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `hp` int(11) NOT NULL,
  `hp_max` int(11) NOT NULL,
  `attack` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `gold` int(11) NOT NULL,
  `fk_skill_id1` int(11) NOT NULL,
  `fk_skill_id2` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `monster`
--

INSERT INTO `monster` (`id`, `name`, `hp`, `hp_max`, `attack`, `exp`, `gold`, `fk_skill_id1`, `fk_skill_id2`) VALUES
(1, 'Troll', 100, 100, 20, 20, 20, 1, 2),
(2, 'Poulet', 50, 50, 10, 10, 10, 7, 6);

-- --------------------------------------------------------

--
-- Table structure for table `quest`
--

CREATE TABLE IF NOT EXISTS `quest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(80) NOT NULL,
  `x` float NOT NULL,
  `y` float NOT NULL,
  `goal_x` float NOT NULL,
  `goal_y` float NOT NULL,
  `fk_monster_id` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `gold` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `quest`
--

INSERT INTO `quest` (`id`, `name`, `description`, `x`, `y`, `goal_x`, `goal_y`, `fk_monster_id`, `exp`, `gold`) VALUES
(1, 'La quete facile', 'Première quête du jeu', 1.002, 1.003, 2.003, 2.003, 1, 30, 30);

-- --------------------------------------------------------

--
-- Table structure for table `skill`
--

CREATE TABLE IF NOT EXISTS `skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `damage` int(11) NOT NULL,
  `mp_cost` int(11) NOT NULL,
  `heal` int(11) NOT NULL,
  `description` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `skill`
--

INSERT INTO `skill` (`id`, `name`, `damage`, `mp_cost`, `heal`, `description`) VALUES
(1, 'Boule de feu', 10, 10, 0, 'Crache une boule de feu'),
(2, 'Boule de glace', 10, 10, 0, 'Crache une boule de glace'),
(3, 'Coup de poing', 5, 5, 0, 'Donne un coup de poing'),
(4, 'Coup fatal', 15, 15, 0, 'Donne un coup de point puissant'),
(5, 'Laser', 15, 15, 0, 'Lance un laser'),
(6, 'Souffle de la mort', 15, 15, 0, 'Souffle un vent mortel'),
(7, 'Pied puant', 8, 8, 0, 'Fait sentir ses pieds puants');

-- --------------------------------------------------------

--
-- Table structure for table `success`
--

CREATE TABLE IF NOT EXISTS `success` (
  `fk_user_id` int(11) NOT NULL,
  `nb_kill` int(11) NOT NULL,
  `quest_done` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `success`
--

INSERT INTO `success` (`fk_user_id`, `nb_kill`, `quest_done`) VALUES
(1, 3, 0),
(2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE IF NOT EXISTS `token` (
  `id` int(11) NOT NULL,
  `tmp_token` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(30) NOT NULL,
  `nickname` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `x` float NOT NULL,
  `Y` float NOT NULL,
  `token_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `mail`, `nickname`, `password`, `x`, `Y`, `token_id`) VALUES
(1, 'jonathanmail@mail.com', 'jonathan', 'trieu', 1.0232, 1.02322, 1),
(2, 'guillaumemail@mail.com', 'guillaume', 'tsai', 1.11, 1.0232, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_skill`
--

CREATE TABLE IF NOT EXISTS `user_skill` (
  `fk_user_id` int(11) NOT NULL,
  `fk_skill_id1` int(11) NOT NULL,
  `fk_skill_id2` int(11) NOT NULL,
  `fk_skill_id3` int(11) NOT NULL,
  `fk_skill_id4` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_skill`
--

INSERT INTO `user_skill` (`fk_user_id`, `fk_skill_id1`, `fk_skill_id2`, `fk_skill_id3`, `fk_skill_id4`) VALUES
(1, 1, 2, 3, 4),
(2, 1, 2, 5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `user_stat`
--

CREATE TABLE IF NOT EXISTS `user_stat` (
  `fk_user_id` int(11) NOT NULL,
  `hp` int(11) NOT NULL,
  `mp` int(11) NOT NULL,
  `hp_max` int(11) NOT NULL,
  `mp_max` int(11) NOT NULL,
  `attack` int(11) NOT NULL,
  `def` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `gold` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_stat`
--

INSERT INTO `user_stat` (`fk_user_id`, `hp`, `mp`, `hp_max`, `mp_max`, `attack`, `def`, `lvl`, `exp`, `gold`) VALUES
(1, 90, 90, 100, 100, 50, 45, 9, 0, 1234),
(2, 80, 80, 100, 100, 30, 30, 5, 0, 1200);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
