<?php

/**

AUTHOR: Jonathan TRIEU, EPITECH 2017

**/

	$link = mysqli_connect('localhost', 'root', 'root', '2017_streetrpg') or die("Erreur de connexion au serveur");
	if (!$link)
		echo mysqli_error();

	$id = $_GET['id'];
	echo "ID Skill : " . $id . "<br /><br />";

	// Skill TABLE
	$q = mysqli_query($link, "SELECT * FROM skill WHERE id = '" . $id . "'");
	while($e = mysqli_fetch_assoc($q))
		$output = $e;
	$result = json_encode($output);
	$result = str_replace("{", "", $result);
	$result = str_replace("}", "", $result);
	$skill_tabs = explode(",", $result);
	
	echo "Skill information :<br />";

	foreach ($skill_tabs as $skill_tab) {
		$skill_tab = str_replace('"', "", $skill_tab);
		$split = explode(":", $skill_tab);
    	$skill_array[$split[0]] = $split[1];
		echo $split[0] . " => " . $split[1] . "<br />";
	}
	echo "<br />";

	//SKILL
	$array = array("damage" => $skill_array["damage"], "mp_cost" => $skill_array["mp_cost"], "heal" => $skill_array["heal"],
			"description" => $skill_array["description"]);
	$skill = array("skill" => $array);
	echo "<br />";
	echo json_encode($skill, JSON_FORCE_OBJECT);
	mysqli_close();
?>